import { Vehicle } from "./vehicle.js";
import { Car } from "./car.js";
import { Motorbike } from "./motorbike.js";

const myVehicle = new Vehicle('Motorcycle', 'Honda',);
myVehicle.print();

//instanceof để kiểm tra đối tượng
console.log(myVehicle instanceof Vehicle); // Output: true


const myCar = new Car('Honda', 'City');
myCar.honk();
console.log(myCar instanceof Car); //True


const myMotorbike = new Motorbike('Honda', 'CBR');
myMotorbike.honk();

myMotorbike.print();

console.log(myMotorbike instanceof Motorbike); // true
