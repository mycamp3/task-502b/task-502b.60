class Vehicle {
    // Hàm khởi tạo (constructor)
    constructor(type, brand) {
        this.type = type;
        this.brand = brand;
    }

    // print() in ra thông tin của phương tiện
    print() {
        console.log(`Type: ${this.type}, Brand: ${this.brand}`);
    }
}

export { Vehicle}