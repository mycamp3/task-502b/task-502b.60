class Car {
    constructor(parambrand, parammodel) {
        this.model = parammodel;
        this.brand = parambrand;
    }

    // print() in ra thông tin của phương tiện
    honk() {
        console.log("Beep!!!");
    }
}
export { Car}