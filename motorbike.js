class Motorbike {
    constructor(brand, model, year) {
        this.brand = brand;
        this.model = model;
    }

    honk() {
        console.log('Beep beep!');
    }

    print() {
        console.log(`Brand: ${this.brand}, Model: ${this.model}`);
    }
}
export { Motorbike}